import React, { Component } from 'react';
import styled from 'styled-components';
import { Form, Input, Select } from 'components/forms';

class AddLink extends Component {
  selectOptions = [
    { value: 'Video' },
    { value: 'Tutorial' },
    { value: 'Article' },
    { value: 'StackOverflow' },
    { value: 'Github' },
    { value: 'Reddit' },
    { value: 'Other' },
  ];

  render() {
    return (
      <Container>
        <h2>Add Link</h2>
        <Form>
          <Input placeholder="Add url" />
          <Input placeholder="Title" />
          <Select options={this.selectOptions} />
        </Form>
      </Container>
    );
  }
}

const Container = styled.div`
  padding: 3rem 0;

  h2 {
    margin-bottom: 1.2rem;
  }

  form {
    display: flex;
    flex-direction: column;
  }
`;

export default AddLink;
