import React from 'react';
import styled from 'styled-components';

const Input = ({onChange, placeholder}) => (
  <StyledInput placeholder={placeholder}/>
);

const StyledInput = styled.input`
  margin-bottom: 1.6rem;
  max-width: 40rem;
  padding: 1rem;
  border: 1px solid var(--gray-line);
  border-radius: var(--border-radius);
  font-size: var(--fs-medium);
`;



export default Input;