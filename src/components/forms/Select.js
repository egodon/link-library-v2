import React from 'react';
import PropTypes from 'prop-types';

const Select = ({ options }) => (
  <select>
    {options.map((option) => (
      <option value={option.value.toLowerCase()}>{option.value}</option>
    ))}
  </select>
);

Select.propTypes = {
  options: PropTypes.array.isRequired,
};

export default Select;
